console.log("Hello World!");

// Repetition Control Structure (Loops)
    // Loops are one of the most important feature that programming must have
    // It lets us execute code repeatedly in a pre-set number or maybe forever

    /* 
        Mini Activity
        1. Create a function named greeting() and display "Hi, Batch 211!"
        using console.log inside the function
        2. Invoke the greating () function 10 times
        3. Take a screenshot of your work's console and send it to the batch hangouts
    
    */

    function greeting() {
        console.log("Hi, Batch 211!");        
    };

    /* greeting();
    greeting();
    greeting();
    greeting();
    greeting();
    greeting();
    greeting();
    greeting();
    greeting();
    greeting(); */

    // or we can just use loops

    /* let countNum = 5;

    while(countNum !== 0) {
        console.log("This is printed inside the loop: " + countNum);
        greeting()
        countNum --;
    }; */

    // While loop

    /* 
        A while loop takes in an expression/condition If the condition evaluates to true, the statements inside the code block will be executed.
    
    */
   /* 
        Syntax
            while(expression/condition) {
                statement/code block
                final expression ++/-- (iteration)
            }

            -expression/condition - this are the unit of code that is being evaluated in our loop
            Loop will run while the condition/expression is true

            -statement/code block - code/instructions that will be executed several times

            -finalExpression - indicates how to advance the loop

    */

    /* let count = 5;
    // While the value of count is not equal to 0
    while(count !== 0) {
        // the current value of count is printed out
        console.log("While: " + count);
        // decreases the value of count by 1 after every iteration to stop the loop when it reaches 0
        count --;
    }; */

    /* let num = 20;

    while(num !== 0) {
        console.log("While: num " + num);
        num --;
    }; */

    /* let digit = 5;

    while(digit !== 20) {
        console.log("While: digit " + digit);
        digit ++;
    }; */

    /* let num1 = 1
    while(num1 === 2){
        console.log("While: num1 " + num1);
        num --;
    }; */

    // Do While Loop

    /* 
        - A do-while loop works a lot like the while loop
        But unlike while loops, do-while loops guarantee that the code will be executed at least once
    
    */

    /* 
        Syntax
        do {
            statement/code block
            finalExpression ++/--
            
        } while (expression/condition)
    
    */
   /* 
   How the Do While Loop Works:
   1. The statements in the "do" block executes once
   2. The message "Do While: " + number will be printed out in the console
   3. After executing once, the while statement will evaluate whether to run the next iteration of the loop based on the given expression/condition
   4. If the expression/condition is not true, another iteration of the loop will be executed and will be repeated until the condition is met
   5. If the condition is true, the loop will stop
   
   */

   /*  let number = Number(prompt("Give me a number"));
    do {
        console.log("Do while: " + number)
        number += 2;
    } while (number < 10); */

    // For loop
    // A For Loop is more flexible that while and do-while loops
    /* 
        It consists of three parts:
        1. Initialization value that will track the progression of the loop
        2. Expression/Condition that will be evaluated which will determine if the loop will run one more time
        3. finalExpression indicates how to advance the loop
    
    */

    /* 
        Syntax

        for (initialization; expression/condition; finalExpression) {
            statement
        }
    
    */

    for (let count = 0; count <= 20; count ++){
        console.log("For Loop: " + count);
    };

    /* 
        Mini Activity
        Refactor the code above that the loop will only print the even numbers
        Take a screenshot of your work and sent it to our batch chat
    
    */

    for (let count = 0; count <= 20; count ++) {        
        if(count % 2 === 0 && count != 0) {
        console.log("Even: " + count); 
        }       
    }

    let myString = "Camille Doroteo";
    // characters in strings may be counted using the .length property
    // strings are special compared to other data types
    console.log(myString.length);

    // Accessing elements of a string
    // Individual characters of a string may be accessed using its index number
    // the first character in a string corresponds to the number 0, the next is 1...

    /* console.log(myString[2]);
    console.log(myString[0]);
    console.log(myString[8]);
    console.log(myString[14]); */

    /* for(let x = 0; x < myString.length; x++) {
        console.log(myString[x])
    }; */

    /* 
        Mini Activity
        create a variable named myFullName
        create a for loop
                  
    */

    let myFullName = "Rowell Verdejo"
    let myNewName = "";
    
    for(let i = 0; i < myFullName.length; i++) {
        if(
            myFullName[i].toLowerCase() == "a" ||
            myFullName[i].toLowerCase() == "e" ||
            myFullName[i].toLowerCase() == "i" ||
            myFullName[i].toLowerCase() == "o" ||
            myFullName[i].toLowerCase() == "u"
        ) {
            // if the letter in the name is a vowel, it will print an empty string
        console.log("")}
        else {
            // Print in the console all non-vowel characters in the name
            console.log(myFullName[i])
            myNewName += myFullName[i]; //concatenate
        }
    };

    console.log(myNewName);
    // concatenate the characters based on the given condition

    // Continue and break statements

    /* 
        The continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block

        The break statement is used to terminate the current loop once a match has been found
    
    */

    for(let count = 0; count <=20; count++) {
        if(count % 2 === 0){
            console.log("Even Number")
            continue;
            // console.log("Hello")
        }
        console.log("Continue and Break: " + count);

        if(count>10) {
            // tells the code to terminate/stop the loop even if the expession/condition of the loop defines that it should execute so long as the value of count is less than or equal to 20
            // number values after 10 will no longer be printed
            break;
        }
    };

    let name = "alexandro";

    for (let i = 0; i <name.length; i++) {
        // the current letter is printed out based on its index
        console.log([i]);

        if (name[i].toLowerCase() === "a"){
            console.log("Continue to the next iteration");
            continue;
        }

        if (name[i].toLowerCase() == "d") {
            break;
        }
    }